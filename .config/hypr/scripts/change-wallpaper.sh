#!/bin/bash

directory=/my/wallpaper/directory

monitor=`hyprctl monitors | grep Monitor | awk '{print $2}' | shuf -n 1`

if [ -d "$directory" ]; then
    random_background=$(ls $directory/* | shuf -n 1)

    hyprctl hyprpaper unload all
    hyprctl hyprpaper preload $random_background
    hyprctl hyprpaper wallpaper "$monitor, $random_background"

fi
